package workbench.common.locks

class UsageLockKey internal constructor(val power: Long, unlock: suspend () -> Unit) : Lock(unlock)
