package workbench.common.locks

enum class UsageLockPolicy {

    /**
     * First In First Out
     */
    FIFO,

    /**
     * Last In First Out
     */
    LIFO,

    /**
     * Sort by Power
     */
    LowToHigh,

    /**
     * Sort by Power
     */
    HighToLow
}
