package workbench.common.locks

import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.sync.Semaphore


class GroupLock {
    private val lock = Semaphore(1)
    private val locks = HashMap<String, Semaphore>()

    suspend fun lock(vararg keys: String): Lock {
        lock.acquire()
        val l = mutableMapOf<String, Semaphore>()
        keys.forEach { l[it] = locks.computeIfAbsent(it) { _ -> Semaphore(1) }  }
        lock.release()
        coroutineScope { l.map { async { it.value.acquire() } }.awaitAll() }
        return Lock {
            lock.acquire()
            l.forEach { it.value.release(); if (it.value.availablePermits > 0) locks.remove(it.key) }
            lock.release()
        }
    }

    companion object {
        private val lock = Semaphore(1)
        private val locks = HashMap<String, Semaphore>()

        suspend fun lock(vararg keys: String): Lock {
            lock.acquire()
            val l = mutableMapOf<String, Semaphore>()
            keys.forEach { l[it] = locks.computeIfAbsent(it) { _ -> Semaphore(1) }  }
            lock.release()
            coroutineScope { l.map { async { it.value.acquire() } }.awaitAll() }
            return Lock {
                lock.acquire()
                l.forEach { it.value.release(); if (it.value.availablePermits > 0) locks.remove(it.key) }
                lock.release()
            }
        }
    }
}
