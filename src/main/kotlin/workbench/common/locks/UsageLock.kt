package workbench.common.locks

import kotlinx.coroutines.sync.Semaphore

class UsageLock(val maxPower: Long, val policy: UsageLockPolicy = UsageLockPolicy.FIFO, val enforceLevel: Int = Int.MAX_VALUE) {
    private val semaphore = Semaphore(1)
    private val queue = mutableListOf<Pair<Long, Semaphore>>()
    private var usage = 0L

    init {
        if (maxPower < 0) throw Exception("Max-Power can not be Negative")
        if (enforceLevel < 1) throw Exception("EnforceLevel can not be lower as 1")
    }

    suspend fun runWith(power: Long, run: suspend () -> Unit) {
        val key = claimUsage(power)
        try {
            run.invoke()
        }catch (ex: Exception) {
            throw ex
        }finally {
            key.unlock()
        }
    }

    suspend fun claimUsage(power: Long): UsageLockKey {
        if (power < 0) throw Exception("Power can not be Negative")
        if (power > maxPower) throw Exception("Power is higher as Max-Power")
        semaphore.acquire()
        if (power + usage > maxPower) {
            val s = Semaphore(1)
            s.acquire()
            val pair = Pair(power, s)
            queue.add(pair)
            semaphore.release()
            s.acquire()
            return UsageLockKey(power) { release(power) }
        }
        usage += power
        semaphore.release()
        return UsageLockKey(power) { release(power) }

    }

    private suspend fun release(power: Long) {
        semaphore.acquire()
        usage -= power
        while (queue.isNotEmpty()) {
            val lock = when (policy) {
                UsageLockPolicy.FIFO -> queue.take(enforceLevel).find { it.first + usage <= maxPower }
                UsageLockPolicy.LIFO -> queue.takeLast(enforceLevel).reversed().find { it.first + usage <= maxPower }
                UsageLockPolicy.HighToLow -> queue.sortedBy { -it.first }.take(enforceLevel).find { it.first + usage <= maxPower }
                UsageLockPolicy.LowToHigh -> queue.sortedBy { it.first }.take(enforceLevel).find { it.first + usage <= maxPower }
            }
            if (lock == null) break
            lock.second.release()
            usage += lock.first
            queue.remove(lock)
        }
        semaphore.release()
    }

}
