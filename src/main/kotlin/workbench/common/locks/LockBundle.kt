package workbench.common.locks

import kotlinx.coroutines.sync.Semaphore

class LockBundle {
    private val lock = Semaphore(1)
    private val locks = HashMap<String, Semaphore>()

    suspend fun lock(key: String): Lock {
        lock.acquire()
        val s = locks.computeIfAbsent(key) { _: String -> Semaphore(1) }
        lock.release()
        s.acquire()
        return Lock {
            lock.acquire()
            s.release()
            if (s.availablePermits > 0) locks.remove(key)
            lock.release()
        }
    }

    fun getSize(): Int {
        return locks.size
    }

    companion object {
        private val lock = Semaphore(1)
        private val locks = HashMap<String, Semaphore>()

        suspend fun lock(key: String): Lock {
            lock.acquire()
            val s = locks.computeIfAbsent(key) { _: String -> Semaphore(1) }
            lock.release()
            s.acquire()
            return Lock {
                lock.acquire()
                s.release()
                if (s.availablePermits > 0) locks.remove(key)
                lock.release()
            }
        }

        fun getSize(): Int {
            return locks.size
        }
    }
}
