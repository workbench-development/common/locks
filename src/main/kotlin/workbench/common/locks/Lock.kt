package workbench.common.locks

open class Lock internal constructor(private val unlock: suspend () -> Unit ) {

    suspend fun unlock() {
        unlock.invoke()
    }
}
